{
  'targets': [
    {
      'target_name': 'bindings',
      'include_dirs': ['/usr/local/include/lame', '/usr/local/include'],
      'libraries': ['-L/usr/local/lib -lmpg123'],
      'sources': [
        'src/bindings.cc',
        'src/node_lame.cc',
        'src/node_mpg123.cc'
      ],
    }
  ]
}
